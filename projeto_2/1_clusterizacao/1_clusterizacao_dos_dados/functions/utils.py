#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from osgeo import gdal, gdal_array


def write_georef_labels(original_image_path: str, georef_labels_path: str, 
                            data: np.array) -> None:
    """Função para escrever um GeoTIFF com as *labels* de classificação do KMeans
    
    Args
        original_image_path (str): Caminho da imagem utilizada na clusterização
        georef_labels_path (str): Caminho absoluto para o arquivo que será gerado
        data (np.array): Dados (labels) a serem salvas
    Return
        None
    """
    
    # Carregando os dados
    ds = gdal.Open(original_image_path)
    band = ds.GetRasterBand(1)
    arr = band.ReadAsArray()
    [cols, rows] = arr.shape
    driver = gdal.GetDriverByName("GTiff")
    
    # Criando o arquivo e definindo a georreferência
    outdata = driver.Create(georef_labels_path, rows, cols, 1, gdal.GDT_UInt16)
    outdata.SetGeoTransform(ds.GetGeoTransform())
    outdata.SetProjection(ds.GetProjection())
    outdata.GetRasterBand(1).WriteArray(data)
    outdata.GetRasterBand(1).SetNoDataValue(-9999) 
    outdata.FlushCache() 
    
    outdata, band, ds = np.repeat(None, 3)


def load_stacked_image(path: str, layers: int):
    """Função para carregar uma imagem com N camadas
    
    Args:
        path (str): Endereço absoluto para o arquivo a ser carregado
        layers (int): Quantidade de camadas que a imagem contém
    
    Returns:
        Dict: Dicionário contendo duas chaves
            ``reshaped_image``: Stack de imagem carregado transformado em vetor 
            ``original_image``: Imagem carregada e empilhada
    """
    
    dataset = gdal.Open(path, gdal.GA_ReadOnly)

    img = np.zeros((dataset.RasterYSize, dataset.RasterXSize, dataset.RasterCount),
                   gdal_array.GDALTypeCodeToNumericTypeCode(dataset.GetRasterBand(1).DataType))
    
    for b in range(img.shape[2]):
        img[:, :, b] = dataset.GetRasterBand(b + 1).ReadAsArray()
    shape = (img.shape[0] * img.shape[1], img.shape[2])

    return {"reshaped_image": np.double(img[:, :, : layers].reshape(shape)) / 10000, 
            "original_image": img}


def print_models_inertia(dmodels: dict) -> None:
    """Função para apresentar o WCSS dos modelos presentes no dicionário
    
    Args:
        dmodels (dict): Dicionário de modelos
    Returns:
        None
    """
    
    for model in dmodels.keys():
        print(f"{model} - {dmodels[model].inertia_}")
