#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np


def create_window_in_image(x: int, y: int, image: np.array) -> np.array:
    """Cria uma janela 10x10 sobre uma imagem a partir de um pixel
    
    Args:
        x (int): Posição X do pixel de referência
        y (int): Posição Y do pixel de referência
        image (np.array): Imagem onde será aplicada a janela
    Returns:
        np.array: Janela gerada sobre a imagem
    """
    
    return image[x - 100: x + 100, y - 100: y + 100]


def create_window_in_labels(x: int, y: int, shape: tuple, label: np.array) -> np.array:
    """Cria uma janela 10x10 sobre o vetor de labels do KMeans
    
    Args:
        x (int): Posição X do pixel de referência
        y (int): Posição Y do pixel de referência
        shape (tuple): Shape da imagem de referência
        label (np.array): Label onde será aplicada a janela
    Returns:
        np.array: Janela gerada sobre os labels
    """

    return label.reshape(shape)[x - 100: x + 100, y - 100: y + 100]
