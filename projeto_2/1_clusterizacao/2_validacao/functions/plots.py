#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from matplotlib import colors
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.animation import FuncAnimation
from sklearn.metrics import davies_bouldin_score
from sklearn.cluster import KMeans

import functions.windows as wds

plt.style.use('ggplot')

def kmeans_in_image(image: np.array, model: KMeans, title="Labels plot") -> None:
    """Função para gerar plot das labels de um modelo em uma imagem
    
    Args:
        image (np.array): Vetor contendo a imagem empilhada
        model (KMeans): Modelo KMeans gerado
        title (str): título do plot
    Returns:
        None
    """
    
    plt.figure(figsize=(10, 10))

    labels = model.labels_
    # labels = np.where(labels == 0, labels, 99)

    x_cluster = labels.reshape(image[:, :, 0].shape)
    plt.imshow(x_cluster)
    
    plt.title(title)
    plt.grid(False)
    plt.show()


def kmeans_in_gif(image: np.array, models: list, filename: str) -> None:
    """Função para transformar os resultados de clusterização KMeans em um GIF
        
    Args:
        imagem (np.array): Vetor contendo a imagem empilhada
        models (list): Lista contendo modelos KMeans gerados
        filename (str): Nome do arquivo que será salvo
    Returns:
        None
    """
    
    fig, ax = plt.subplots(figsize=(10, 10))
    fig.grid(False)
    fig.set_tight_layout(True)
    
    def update(i):
        label = "Clusters: {}".format(models[i].n_clusters)
        X_cluster = models[i].labels_.reshape(image[:, :, 0].shape)
        
        im_plot = ax.imshow(X_cluster, cmap="hsv")
        ax.set_xlabel(label)
    
        return ax, im_plot
    anim = FuncAnimation(fig, update, frames=np.arange(0, len(models)), interval=2000)
    anim.save('{}.gif'.format(filename), dpi=300, writer='pillow')


def plot_elbow(models: list) -> None:
    """Função para realizar plot do parâmetro de Elbow de um conjunto de modelos
    
    Args:
        models (list): Lista de modelos KMeans gerados
    
    Returns:
        None
    """
    
    wcss, ks = [], []
    for model in models:
        wcss.append(model.inertia_); ks.append(model.n_clusters)
    
    plt.figure(figsize=(10, 10))
    plt.plot(ks, wcss)
    plt.title("Parâmetro Elbow")
    plt.xlabel("Quantidade de Clusters")
    plt.ylabel("WCSS")
    plt.show()


def scatter3(model, title: str, images: list, c: list) -> None:
    """Cria um plot de um Scatter 3D de pixels
    
    Args:
        title (str): Título do plot 
        model (list): Labels resultantes da clusterização
        images (list): Lista com imagens para realização dos plots 3D
        c (list): Lista de cores que devem ser aplicadas em cada classe das labels
    Returns:
        None
    """
    
    n = len(images)
    fig = plt.figure(figsize=(20, 7))
    
    for image, index in zip(images, range(1, n + 1)):
        # Separa os dados presentes na imagem
        imgr = image["reshaped_image"]
        imgo = image["original_image"]
        
        # Clusteriza
        labels = model.predict(imgr)
        clusters = model.cluster_centers_
        
        # Separa as imagens para plot
        r = imgo[:, :, 0]
        g = imgo[:, :, 1]
        b = imgo[:, :, 2]

        axis = fig.add_subplot(1, n, index, projection = "3d")

        # axis.scatter(*clusters[0], marker="x", s=300, c="black")
        # axis.scatter(*clusters[1], marker="x", s=300, c="black")
        # axis.scatter(*clusters[2], marker="x", s=300, c="black")

        axis.scatter(r, g, b, c=labels.ravel(), cmap=colors.ListedColormap(c), marker="o")
        plt.title(f"{title} - Ponto {str(index)}")
        axis.set_xlabel("Red")
        axis.set_ylabel("Green")
        axis.set_zlabel("Blue")
    plt.show()
