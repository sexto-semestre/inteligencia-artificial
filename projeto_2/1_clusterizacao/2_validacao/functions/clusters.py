#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
from sklearn import cluster


def kmeans(image: np.array, n: int) -> list:
    """Função para aplicar o KMeans em uma imagem
        
    Args:
        imagem (np.array): Vetor contendo a imagem
        n (int): Quantidade de clusters que devem ser gerados (Considerando o intervalo: 2 ... N)
    
    """
    
    models = []
    
    for i in range(2, n + 1):
        # Clusteriza
        k_means = cluster.KMeans(n_clusters=i)
        k_means.fit(image)
        
        # Salvando modelo
        models.append(k_means)
    return models


def mkmeans(image: np.ndarray, initializers: list, k_values: list, 
                             tols: list, maxiters: list, ninits: list) -> list:
    """Função para realizar diversas execuções do KMeans
    
    Args:
        image (np.ndarray): Imagem utilizada na classificação
        
        initializers (list): Lista com nomes dos métodos de inicialização
        
        k_values (list): Lista com as quantidades de K para cada rodada
        
        tols (list): Lista de limites para cada rodada
        
        maxiters (list): Lista de limite de iteração para cada rodada
        
        ninits (list): Lista de quantidades de inicializadores para os k-centroids

    Returns:
        list: Lista com cada modelo de KMeans gerado
    """
    
    models = []
    for initializer, k, tol, max_iter, n_init in zip(
        initializers, k_values, tols, maxiters, ninits        
    ):
        models.append(cluster.KMeans(init=initializer, n_clusters=k, tol=tol, 
                random_state=999, max_iter=max_iter, n_init=n_init).fit(image))
    return models
