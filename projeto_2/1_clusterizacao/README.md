# Clusterização de imagens Sentinel-2/MSI

Esta seção do trabalho realiza a clusterização de imagens Sentinel-2/MSI para a identificação de campos de mineração em diferentes imagens

## Separação

A separação desta atividade de clusterização está dividida nas seguintes partes:

- [1_clusterizacao_dos_dados](/1_clusterizacao_dos_dados): Documentos para a aplicação de algoritmos de clusterização em imagens Sentinel-2/MSI para a identificação de campos de mineração;
- [2_validacao](/2_validacao): Documentos para validar os resultados da aplicação dos algoritmos de clusterização.
