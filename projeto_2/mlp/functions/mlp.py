# -*- coding: utf-8 -*-

import pandas as pd 
import seaborn as sn
import ClassificationUtils as utils
import matplotlib.pyplot as plt
import sklearn.neural_network as mlp
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split

PATH_DADOS = "/home/weslei/Documents/Workspaces/IA/inteligencia-artificial/projeto_2/mlp/dados/valores_unificados"

data = pd.read_csv(f"{PATH_DADOS}.csv", sep=",", encoding="UTF-8")
# lista com indexes das bandas
INDEX = ["B8","B4","B3"]
CLASS = ["classe"]
CONFUSION_MATRIX_INDEX = ["mineracao", "vegetacao", "agua"]

## separa o dataset para treino e teste
X_train, X_test, y_train, y_test = train_test_split(data[INDEX], data[CLASS], test_size=0.30, random_state=50)

# cria instancia de MLPClassifier com os parametros que deixaram a classificacao melhor
classifier = mlp.MLPClassifier(hidden_layer_sizes = (100, 100), activation = "tanh", solver="sgd", batch_size=41, learning_rate="adaptive", max_iter=200, random_state=0, verbose=True)

# treina o modelo. Aprende e devolve um modelo generalizado para classificar imagens do Landsat-8
model_fited = classifier.fit(X_train, y_train)

# verifica acuracia da rede
score_result = model_fited.score(X_test, y_test)
print(f"MLP Accuracy: {score_result}")
# tenta adivinhar quais pixels sao agua e quais nao sao
y_predict = model_fited.predict(X_test)

# gera matriz de confusao
confusion_matrix = confusion_matrix(y_predict, y_test)

# cria dataframe com as classes "agua" e "nao agua"
data_frame_confusion_matrix = pd.DataFrame(confusion_matrix, index = CONFUSION_MATRIX_INDEX, columns = CONFUSION_MATRIX_INDEX )

# define tamanho da figura do plot
plt.figure(figsize=(10,10))

# exibe figura
sn.heatmap(data_frame_confusion_matrix, annot=True)

IMAGE_PATH = "/home/weslei/Documents/Workspaces/IA/Images/Teste/validacao"

utils.load_plot_classified_image(f"{IMAGE_PATH}.tif", model_fited, INDEX, ten_thousand_division=True, figsize=(10,10))

#stacked_image = utils.load_stacked_image(f"{IMAGE_PATH}.tif", ten_thousand_division=True)
#df = utils.ndarrayToDataFrame(stacked_image["reshaped_image"], INDEX)
#predicted_result = model_fited.predict(df)
#result_encoded = utils.encodeLabels(predicted_result)

#utils.save_georef_labels(f"{IMAGE_PATH}.tif", f"{IMAGE_PATH}_SALVA.tif", result_encoded)