#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    
    Classificação de imagens de rio
    
    Inteligência artifical - Trabalho II
"""

import pandas as pd 
import seaborn as sn
import matplotlib.pyplot as plt
import sklearn.neural_network as mlp
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import ClassificationUtils
import numpy as np

data = pd.read_csv("/home/weslei/Documents/Workspaces/IA/inteligencia-artificial/projeto_2/mlp/mlp/dados-agua-nao-agua.csv", sep=",", encoding="UTF-8")
# lista com indexes das bandas
INDEX = ["B4","B5","B6"]

#separa o dataset para treino e teste
X_train, X_test, y_train, y_test = train_test_split(data[INDEX], data["classe"], test_size=0.30, random_state=50)

# cria instancia de MLPClassifier com os parametros que deixaram a classificacao melhor
classifier = mlp.MLPClassifier(hidden_layer_sizes = (100), activation = "logistic", max_iter=316, random_state=50, verbose=False)

# treina o modelo. Aprende e devolve um modelo generalizado para classificar imagens do Landsat-8
model_fited = classifier.fit(X_train, y_train)

# verifica acuracia da rede
score_result = model_fited.score(X_test, y_test)
print("MLP Accuracy: {}".format(score_result))
# tenta adivinhar quais pixels sao agua e quais nao sao
y_predict = model_fited.predict(X_test)

# gera matriz de confusao
confusion_matrix = confusion_matrix(y_predict, y_test)

# cria dataframe com as classes "agua" e "nao agua"
data_frame_confusion_matrix = pd.DataFrame(confusion_matrix, index = ["agua", "nao agua"], columns = ["agua", "nao agua"])

# define tamanho da figura do plot
plt.figure(figsize=(10,10))

# exibe figura
sn.heatmap(data_frame_confusion_matrix, annot=True)

##########################
#   Mississippi river   #
#########################

# cria instancia da classe com facilidades para o processo de classificacao em outras imagens
utils = ClassificationUtils()

# define caminho absoluto para imagens
IMG_PATH = "/home/weslei/Documents/Workspaces/IA/Sentinel Image/valdation-image/LC08_L1TP_024033_20190421_20190507_01_T1/LC08_L1TP_024033_20190421_20190507_01_T1_WATER_VIEW_CROP.TIF"

# carrega imagem e cria dois atributos na classe ClassificationUtils, reshaped_image e original_image
utils.load_stacked_image(IMG_PATH, 3)

# cria DataFrame com os dados da imagem carregados em um np.ndarray
dfTestImage = utils.ndarrayToDataFrame(utils.reshaped_image, INDEX)

# testa a rede com os dados da imagem carregada
result = model_fited.predict(dfTestImage)

# converte o resultado recebido pela rede com as propriedades da imagem para que o plot seja gerado
classified_reshaped = utils.reshape_encode_labels(result, utils.original_image.shape[0], utils.original_image.shape[1])

# define tamanho da imagem
plt.figure(figsize=(15, 15))
# plotando imagem classificada
plt.imshow(np.array(classified_reshaped, dtype = np.uint8 ), cmap='Blues')

#############################################
#   West Side National Park - The Bahamas   #
#############################################

IMG_PATH = "/home/weslei/Documents/Workspaces/IA/Sentinel Image/valdation-image/LC08_L1TP_013043_20190424_20190508_01_T1/LC08_L1TP_024033_20190421_20190507_01_T1_WATHER_VIEW_CROP.TIF"

utils.load_stacked_image(IMG_PATH, 3)

dfTestImage = utils.ndarrayToDataFrame(utils.reshaped_image, INDEX)

result = model_fited.predict(dfTestImage)

classified_reshaped = utils.reshape_encode_labels(result, utils.original_image.shape[0], utils.original_image.shape[1])

plt.figure(figsize=(15, 15))

plt.imshow(np.array(classified_reshaped, dtype = np.uint8 ), cmap='Blues')