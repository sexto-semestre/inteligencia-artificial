#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd

BANDS = ["B8", "B4", "B3"]
to_double = lambda x: np.double(x) / 10000

def organize_(bnames: list, path: str, class_name: str) -> pd.DataFrame:
    """Função para carregar e organizar os dados
    
    Args:
        bnames (list): Lista com os nomes das bandas presentes no arquivo
        path (str): Caminho absoluto até o arquivo a ser carregado
        class_name (str): Nome da classe dos dados presentes no arquivo
    Returns:
        pd.DataFrame: Tabela com os dados organizados
    """
    
    df = pd.read_csv(path, names = bnames)
    df = df.iloc[1:].apply(to_double)
    df["classe"] = class_name
    return df

agua = organize_(BANDS, "agua.csv", "agua")
mineracao = organize_(BANDS, "mineracao.csv", "mineracao")
vegetacao = organize_(BANDS, "vegetacao.csv", "vegetacao")

pd.concat([
        agua, mineracao, vegetacao
]).to_csv("valores_unificados.csv")
