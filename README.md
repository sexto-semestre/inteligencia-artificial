# Inteligência artificial - Fatec-SJC :moyai:

Repositório com o projeto do primeiro bimestre na matéria de inteligência artificial da Fatec-SJC.

## Grupo :dragon_face:

O grupo que desenvolveu este trabalho é composto pelos seguintes integrantes:
- Carlos Augusto dos Santos Neto;
- Felipe Menino Carlos;
- Mauricio Kiyoshi Yassunaga;
- Weslei Luiz de Paula Pinto.

## Conteúdo :balloon:

Neste repositório há os seguintes conteúdos:
- Sistema especialista
    - Sistema para a identificação das melhores empresas de telefonia.
- Árvore de decisão
    - Árvore de decisão para classificação de imagens Landsat-8/OLI.
- Regressão linear
    - Previsão da quantidade de clientes em uma loja com base no ambiente onde ela está inserida.
