function [result] = getPointInImage( nPoints, image, label )
%getPointInImage Função utilizada para capturar índices da matriz da imagem
%através do ponto selecionado
%   nPoints: Quantidade de pontos que será adquirida
%   image: Matriz da imagem a ser utilizada
%   label: Nome da classe que está sendo buscada
    
    result = {};
    positions = [];

    figure() % Cria figura na memória
    for i=1:1:nPoints
        imshow(image, []); title([label '- ' num2str(i) '/' ...
                                                        num2str(nPoints)]);
        % Criando ROI
        point = impoint;
        mask = point.createMask();
        [row, column] = find(mask == 1);
                     
        % Salva 8 posições (A selecionada pelo usuário e modificações desta)
        positions = [positions; [row, column]];
        positions = [positions; [row - 1, column - 1]];
        positions = [positions; [row + 1, column + 1]];
        positions = [positions; [row, column + 1]];
        positions = [positions; [row + 1, column]];
        positions = [positions; [row + 2, column]];
        positions = [positions; [row, column + 2]];
        positions = [positions; [row + 2, column + 2]];
    end; close();
    
    % Salva os resultados na struct
    result.label = label;
    result.points = positions;
end
