imagens_utilizadas = { ...
    '/media/felipe/0AF2C7F6F2C7E453/imagens_para_arvore/LC08_L1TP_224063_20180614_20180703_01_T1', ...    
    '/media/felipe/0AF2C7F6F2C7E453/imagens_para_arvore/LC08_L1TP_227062_20180721_20180731_01_T1', ...    
    '/media/felipe/0AF2C7F6F2C7E453/imagens_para_arvore/LC08_L1TP_228062_20180930_20181010_01_T1' 
};

pontos_dir_base = './saidas_editadas';

pontos = { ...
    { ...
        fullfile([pontos_dir_base, '/agua_LC08_L1TP_224063_20180614_20180703_01_T1.csv']), ...
        fullfile([pontos_dir_base, '/nagua_LC08_L1TP_224063_20180614_20180703_01_T1.csv']) ...
    }, ... 
    {
        fullfile([pontos_dir_base, '/agua_LC08_L1TP_227062_20180721_20180731_01_T1.csv']), ...
        fullfile([pontos_dir_base, '/nagua_LC08_L1TP_227062_20180721_20180731_01_T1.csv']) ...
    }, ...
    {
        fullfile([pontos_dir_base, '/agua_LC08_L1TP_228062_20180930_20181010_01_T1.csv']), ...
        fullfile([pontos_dir_base, '/nagua_LC08_L1TP_228062_20180930_20181010_01_T1.csv']) ...
    } ...
};

for index_de_imagem=1:1:length(imagens_utilizadas)
    nome_da_figura = strsplit(imagens_utilizadas{1, index_de_imagem}, '/');
    
    disp(nome_da_figura{1, end});
    
    % Carrega a imagem
    im = L8read(imagens_utilizadas{1, index_de_imagem});
   
    % Carregando a tabela de pontos
    tabela_com_agua = readtable(pontos{1, index_de_imagem}{1, 1});
    tabela_sem_agua = readtable(pontos{1, index_de_imagem}{1, 2});
        
    % Gerando os plots
    fig = figure(); imshow(im); axis on; hold on;
    
    for i=1:size(tabela_com_agua, 1)
        plot(tabela_com_agua.y(i), tabela_com_agua.x(i), 'o', 'MarkerSize', 1, 'LineWidth', 5, 'Color', 'Blue');
        plot(tabela_sem_agua.y(i), tabela_sem_agua.x(i), 'o', 'MarkerSize', 1, 'LineWidth', 5, 'Color', 'Red');    
    end
    pause(15);
    
    % Salvando a figura
    saveas(fig, fullfile(['figuras/' nome_da_figura{1, end}  '.tiff']), 'tiffn');
end
