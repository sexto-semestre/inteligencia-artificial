%{
    Script principal para a coleta de pontos em imagens Landsat-8/OLI
%}

imagens_utilizadas = { ...
    '/media/felipe/0AF2C7F6F2C7E453/imagens_para_arvore/LC08_L1TP_228062_20180930_20181010_01_T1', ...    
};

% plot(3500, 3500, 'o', 'MarkerSize', 2, 'LineWidth', 5, 'Color', 'Red');

for index=1:1:size(imagens_utilizadas, 2)
    imagem = imagens_utilizadas{1, index};
    im = L8read(imagem);

    % Capturando os pontos das imagens
    posicoes_agua = getPointInImage(40, im, 'Água');
    posicoes_nagua = getPointInImage(40, im, 'Não-Água');
    
    % Extraíndo nome da imagem
    nome_imagem = strsplit(imagem, '/');
    nome_imagem = nome_imagem{1, 6};
    
    % Salvando os resultados
    struct2csv(posicoes_agua, ...
                            fullfile(['saidas/agua_' nome_imagem '.csv']));
    struct2csv(posicoes_nagua, ...
                            fullfile(['saidas/nagua_'  nome_imagem '.csv']));
end
