import numpy as np
import pandas as pd

from processamento import gera_tabel_de_atributos
from utils import carrega_as_tabelas_de_posicao_de_pixel

index_dos_pixels_classificados = carrega_as_tabelas_de_posicao_de_pixel([
    "../1_escolha_dos_dados/saidas/agua_LC08_L1TP_224063_20180614_20180703_01_T1.csv",
    "../1_escolha_dos_dados/saidas/nagua_LC08_L1TP_224063_20180614_20180703_01_T1.csv"
])
diretorio_das_imagens = "/media/felipe/0AF2C7F6F2C7E453/imagens_para_arvore/LC08_L1TP_224063_20180614_20180703_01_T1"
tabela_de_atributos = gera_tabel_de_atributos(diretorio_das_imagens, 
                            index_dos_pixels_classificados.values)

# Salva os resultados
pd.DataFrame(tabela_de_atributos).to_csv("saidas/tabela_de_atributos_imagem_diferente.csv")
