import pandas as pd


def carrega_todas_as_bandas_da_imagem(diretorio_das_imagens: str):
    """
    """

    import os
    import glob
    from osgeo import gdal

    if not os.path.isdir(diretorio_das_imagens):
        raise NotADirectoryError("Diretório inserido não é válido")

    imagens = glob.glob(os.path.join(diretorio_das_imagens, "*.TIF"))
    if len(imagens) == 0:
        raise RuntimeError("Nenhuma imagem foi econtrada")
    
    for imagem in imagens:
        banda = imagem.split('_')[-1].split('.')[0]
        
        yield {
            "banda": banda,
            "imagem": gdal.Open(imagem).ReadAsArray()
        }


def carrega_as_tabelas_de_posicao_de_pixel(caminho_ate_tabelas):
    """
    """

    tabelas = []

    for caminho in caminho_ate_tabelas:
        tabela = pd.read_csv(caminho)
        tabela.columns = ["classe", "linha", "coluna", "NA"]; del tabela["NA"]
        tabela["classe"].values[:] = tabela["classe"].iloc[0]
        
        tabelas.append(tabela)
    return pd.concat(tabelas, ignore_index=True)
