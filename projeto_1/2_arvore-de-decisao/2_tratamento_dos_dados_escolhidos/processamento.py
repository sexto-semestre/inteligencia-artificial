import numpy as np
from numba import jit
from utils import carrega_todas_as_bandas_da_imagem


def gera_tabel_de_atributos(diretorio_das_imagens: str, 
                                index_dos_pixels_classificados: np.ndarray) -> dict:
    """Função para gerar a tabela com os atributos para classificação

    Args:
        diretorio_das_imagens (str): Diretório onde estão todas as imagens
        index_dos_pixels_classificados (np.ndarray): Índices de cada pixel na tabela de atributos

    Returns:
        dict: Dicionário de atributos
    """

    tabela_de_atributos = {
        "B1": [], "B2": [],
        "B3": [], "B4": [],
        "B5": [], "B6": [],
        "B7": [], "B8": [],
        "B9": [], "B10": [],
        "B11": [], "classe": []
    }

    # A escolha desta implementação foi realizada para consumir menos memória, porém
    # leva mais tempo para o processamento
    primeira_iteracao = True
    for imagem in carrega_todas_as_bandas_da_imagem(diretorio_das_imagens):
        print(f"Processando {imagem['banda']}")
        for posicao_pixel in index_dos_pixels_classificados:
            tabela_de_atributos[imagem["banda"]].append(imagem["imagem"][
                    posicao_pixel[1], posicao_pixel[2]
            ] / 10000)
            
            if primeira_iteracao:
                tabela_de_atributos["classe"].append(posicao_pixel[0])
        primeira_iteracao = False
    return tabela_de_atributos
