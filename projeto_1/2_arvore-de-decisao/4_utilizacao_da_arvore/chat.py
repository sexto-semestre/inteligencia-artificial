import os
import numpy as np
from emoji import emojize
from facade import classifica_imagem

STEP_1_MSG = '''
Olá :snake: \nSeja bem-vindo a aplicação para classificação de imagens Landsat-8/OLI :satellite: \n
O classificador presente nesta ferramenta foi desenvolvido durante a matéria de IA da Fatec SJC. Para começarmos, insira o caminho completo do diretório onde os arquivos das bandas do Landsat-8/OLI estão disponível
'''

STEP_2_MSG = '''
Certo! Insira agora o diretório onde a imagem deve ser salva
'''

caminho_ate_imagem = None
diretorio_de_saida = None

try:
    print(emojize(STEP_1_MSG))
    caminho_ate_imagem = input(":")
    print(emojize(STEP_2_MSG))
    diretorio_de_saida = input(":")

    # Classificando e salvando o resultado
    matriz_classificada = classifica_imagem(caminho_ate_imagem)
    np.save(os.path.join(diretorio_de_saida, "imagem_classificada.npy"), matriz_classificada)

except RuntimeError as e:
    print(emojize(f"{str(e)} :cry:"))

except NotADirectoryError as e:
    print(emojize(f"{str(e)} :collision:"))
