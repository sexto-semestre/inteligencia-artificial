def busca_imagens(diretorio_de_imagens: str) -> dict:
    """Método para buscar imagens dentro do diretório definido pelo usuário

    Args:
        diretorio_de_imagen (str): Diretório das imagens Landsat-8/OLI

    Returns:
        dict: Dicionário com a relação de bandas e imagens
    """

    import os
    import glob

    if not os.path.isdir(diretorio_de_imagens):
        raise NotADirectoryError("Diretório inválido")
   
    imagens = glob.glob(os.path.join(diretorio_de_imagens, "*.TIF"))    
    if len(imagens) == 0:
        raise RuntimeError("Nenhuma imagem foi encontrada!")

    relacao_banda_imagem = {}
    for imagem in imagens:
        banda = imagem.split('_')[-1].split('.')[0]

        relacao_banda_imagem[banda] = imagem
    return relacao_banda_imagem
