import numpy as np
from numba import jit


@jit(nopython=True, parallel=True)
def classifica_imagem(imagem_b3: np.ndarray, imagem_b5: np.ndarray, 
                                                imagem_b6: np.ndarray) -> np.ndarray:
    """Função para classificar de imagens Landsat-8/OLI

    Args:
            imagem_b3 (np.ndarray): Imagem Landsat-8/OLI banda 3
            imagem_b5 (np.ndarray): Imagem Landsat-8/OLI banda 5
            imagem_b6 (np.ndarray): Imagem Landsat-8/OLI banda 6

    Returns:
            np.ndarray: Mascara com a classificação de cada um dos pixels. Sobre os valores da mascara:
                1 -> Água
                2 -> Não-Água

    See:
        A função é decorada com `@jit` para permitir que ela tenha uma execução rápida
    """
    
    mascara = np.zeros(imagem_b5.shape)

    for i in range(0, imagem_b5.shape[0]):
        for j in range(0, imagem_b5.shape[1]):
            pixel_b3 = imagem_b3[i, j]
            pixel_b5 = imagem_b5[i, j]
            pixel_b6 = imagem_b6[i, j]

            # Regras geradas na árvore de decisão (Modelo mln_0)
            if pixel_b5 != 0:
                if pixel_b5 <= 1.011:
                    if pixel_b3 <= 0.742:
                        if pixel_b6 <= 0.541:
                            mascara[i, j] = 1
                        else:
                            mascara[i, j] = 2
                    else:
                        mascara[i, j] = 1
                else:
                    mascara[i, j] = 2       
    return mascara
