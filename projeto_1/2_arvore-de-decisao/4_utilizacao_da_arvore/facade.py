import numpy as np


def classifica_imagem(path: str) -> np.ndarray:
    """Função para a classificação de imagens Landsat-8/OLI
    
    Args:
        path (str): Caminho até o diretório com as bandas da imagen a ser classificada
    Return:
        np.ndarray: Matriz com a classe definida para cada um dos pixels
    """

    from osgeo import gdal
    from utils import busca_imagens

    import matplotlib.pyplot as plt
    import matplotlib.patches as mpatches

    from classificador import classifica_imagem

    imagens = busca_imagens(path)

    # Carregando as imagens
    # XXX A divisão por 10000 é realizada para os valores presentes no pixel serem valores reais
    imagem_b3 = gdal.Open(imagens["B3"]).ReadAsArray() / 10000
    imagem_b5 = gdal.Open(imagens["B5"]).ReadAsArray() / 10000
    imagem_b6 = gdal.Open(imagens["B6"]).ReadAsArray() / 10000

    # Classificando pixel-a-pixel
    mascara_de_classificacao = classifica_imagem(
        imagem_b3, imagem_b5, imagem_b6
    )

    # Criando plot da classificação
    plt.figure(figsize=(8, 8))
    p = plt.imshow(mascara_de_classificacao, cmap="inferno")

    ## Legenda
    nomes = {
        0: 'Água',
        1: 'Não água'
    }

    valores = [1, 2]
    cores = [ p.cmap(p.norm(value)) for value in valores]
    patches = [ mpatches.Patch(color=cores[i], label=f"{nomes[i]}") for i in range(len(valores)) ]
    plt.legend(handles=patches, bbox_to_anchor=(1, 1), loc=2, borderaxespad=.4 ); plt.show()

    return mascara_de_classificacao
