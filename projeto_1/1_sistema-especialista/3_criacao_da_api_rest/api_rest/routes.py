from flask_restful import (
    Resource,   # Classe que possui interfaces para implementar comportamentos de 'http methods' (put, delete, get, post, ...)
)

from api_rest.services import DiscoveryBusinessService

class DiscoveryBusinessRoute(Resource):         # Herda os comportamentos da Classe Resource, disponibilizando a implementacao dos metodos http por nome de funcao    
    
    def __init__(self):
        self.service_discovery = DiscoveryBusinessService()
    
    def get(self, idade, nota, opcao, tempo):   # Quando o endpoint for requisitado via GET, tera o comportamento desta funcao...
        business = self.service_discovery.get_business(idade, nota, opcao, tempo)

        return {
            "empresa": business
        }
