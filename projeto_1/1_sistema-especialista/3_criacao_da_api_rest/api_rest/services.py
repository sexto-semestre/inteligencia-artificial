from especialista_fatec.facade import define_empresa_do_usuario

class DiscoveryBusinessService(object):
    def get_business(self, idade, nota, opcao, tempo):

        result = define_empresa_do_usuario({
            "idade": int(idade),
            "nota": int(nota),
            "opcao": str(opcao),
            "tempo": int(tempo)
        }); 

        return result