from flask import Flask      # importa classe Flask

from flask_restful import (
    Resource,   # Classe que possui interfaces para implementar comportamentos de 'http methods' (put, delete, get, post, ...)
    Api         # Classe que recebera uma instancia de 'Flask', cd
)

from .routes import DiscoveryBusinessRoute

def create_app():
    app = Flask(__name__)   # Declara uma instacia da classe Flask
    api = Api(app)          # Cria uma instancia do flask API

    #http://127.0.0.1:5000/idade=20&nota=5&opcao=Internet&tempo=8
    api.add_resource(       # Associa a regra de negocio descrita na por meio de uma classe que extende 'Resource',e associa a endpoint, neste caso '/discovery_business' 
        DiscoveryBusinessRoute,
        '/idade=<string:idade>&nota=<string:nota>&opcao=<string:opcao>&tempo=<string:tempo>'
    )
 
    return app