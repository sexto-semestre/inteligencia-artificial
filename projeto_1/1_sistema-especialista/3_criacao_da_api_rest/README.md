# Sistema especialista :computer: - API

API Rest para consumo do sistema especialista

## Executando

python3 run.py

## Exemplo de query

```
http://127.0.0.1:5000/idade=20&nota=5&opcao=Internet&tempo=8
```

`OBS`: O exemplo abaixo utiliza o servidor web local.
