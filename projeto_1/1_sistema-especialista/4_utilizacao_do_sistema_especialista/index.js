const bot = require("./bot");
const scopes = require("./scopes");
const messages = require("./messages");
const sleep = require('system-sleep');
const getBusiness = require("./api");

const userInfos = {};
let scope = scopes.intro;

bot.onText(/\/start/, (msg) => {
    const chatId = msg.chat.id;
    
    bot.sendMessage(chatId, messages.start.intro);
    bot.sendMessage(chatId, messages.questions.start); sleep(2);

    // Definindo o escopo inicial para a busca de idades
    scope = scopes.age;
    bot.sendMessage(chatId, messages.questions.age);
});

bot.onText(/\/restart/, (msg) => {
    const chatId = msg.chat.id;

    bot.sendMessage(chatId, "Certo! Vamos recomeçar a pesquisa da melhor empresa para você!"); sleep(2);
    // Iniciando com o escopo de idade novamente
    scope = scopes.age;
    bot.sendMessage(chatId, messages.questions.age);
});

bot.on("message", (msg) => {
    const chatId = msg.chat.id;
    
    // Criando objeto do usuário com suas informações na memória
    if (scope === scopes.intro)
        userInfos[chatId] = {};
    userInfos[chatId][scope] = msg.text;

    switch(scope) {
        case scopes.age:
            scope = scopes.note;
            bot.sendMessage(chatId, messages.questions.note, {
                "reply_markup": {
                    "keyboard": [
                        ["0", "1", "2"],
                        ["3", "4", "5"]
                    ]
                }
            });
            break;
        case scopes.note:
            scope = scopes.time;
            bot.sendMessage(chatId, messages.questions.time, {
                "reply_markup": {
                    "keyboard": [
                        ["0", "1", "2", "3"],
                        ["4", "5", "6", "7"],
                        ["8", "9", "10"]    
                    ]
                }
            });
            break;
        case scopes.time:        
            scope = scopes.opt;
            bot.sendMessage(chatId, messages.questions.opt, {
                "reply_markup": {
                    "keyboard": [
                        ["Internet", "Loja física"], 
                        ["Telefone", "SMS"], 
                        ["Domicílio"]]
                }
            });
            break;
        case scopes.opt:
            scope = scopes.completed;   

            // Recuperando informações do usuário
            const user = userInfos[chatId];
            const businessToUser = getBusiness(user);

            businessToUser.then(r => {
                bot.sendMessage(chatId, `Escolha: ${r.data.empresa}`);
                bot.sendMessage(chatId, messages.finish.completed);
            });
            break;
        case scopes.completed:
            bot.sendMessage(chatId, messages.finish.completed);
            break;
    }
});
