# Bot - Sistema especialista

Este é um pequeno bot criado para utilizar o sistema especialista e a APIRest criada na matéria de inteligência artificial

## Utilização

Para utilizar, altere o arquivo `bot.js` com a informação de seu token 

```js
const token = "TOKEN_DA_API_TELEGRAM";
```

Após fazer isto, instale as dependências com o comando `npm install` e em seguida execute o projeto com o comando `npm run-script runmon`.

`OBS`: Não esqueça de ter a APIRest do sistema especialista rodando em sua máquina!
