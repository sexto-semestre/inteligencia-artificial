const messages = {
    start: {
        intro: "Olá seja bem-vindo, eu vou te ajudar a escolher a melhor empresa de telefonia através de algumas perguntas"
    }, 
    finish: {
        completed: "Você finalizou o teste de busca de empresas, para recomeçar utilize o comando /restart"
    },
    questions: {
        start: "Vamos iniciar a definição da melhor empresa para você através de algumas perguntas",
        age: "Quantos anos você tem ?",
        time: "Uma empresa ideal para você, deve antender seus clientes em até quantas horas após a abertura do chamado ? Isto em uma escala de 0 a 10",
        note: "Se você fosse escolher uma empresa com base na avaliação de outros clientes, qual seria a nota da empresa ideal para você ? Em uma escala de 0 a 5",
        opt: "Qual sua forma de compra favorita ?"
    },
    error: {
        default: "Ops! Algo deu errado... Utilize os comandos para conversar comigo"
    }
};

module.exports = messages;
