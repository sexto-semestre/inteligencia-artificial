const axios = require("axios");

/**
 * Função para consultar a API do sistema especialista e recuperar 
 * a empresa para o cliente
 * 
 * @param user Objeto com as informações do usuário
 * @returns string Nome da empresa indicada para o usuário
 * 
 */
async function getBusiness(user) {
    return await axios(`http://localhost:5000/idade=${user["2"]}&` +
                                `nota=${user["3"]}&opcao=${user["4"]}&` +
                                `tempo=${user["1"]}`); 
};

module.exports = getBusiness;
