const TelegramBot = require("node-telegram-bot-api");

// Link para utilizar o bot: http://t.me/especialista_sexto_bot
const token = "";
const bot = new TelegramBot(token, {polling: true});

module.exports = bot;
