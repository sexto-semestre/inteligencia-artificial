const scopes = {
    intro: 0,
    time: 1,
    age: 2,
    note: 3,
    opt: 4, // Forma de compra
    error: -1,
    completed: 5
}

module.exports = scopes;
