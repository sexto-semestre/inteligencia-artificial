# Sistema especialista :computer:

Sistema especialista para ajudar na seleção da empresa de telecomunicação que melhor se encaixa em seu perfil.

As regras deste sistema especialista foram criadas com base na análise de dados realizado com os dados do `consumidor.gov.br`.

## Instalação :moyai:

Para realizar a instalação, vá para o diretório `instalador` e execute o comando abaixo:

```shell
pip install EspecialistaAPI-0.1-py3-none-any.whl
```

Caso seja necessário gerar um arquivo `.whl` para sua versão do Python você terá de executar o `setup.py` como demonstrado abaixo

```shell
python setup.py sdist bdist_wheel
```

Feito isto, acesse o diretório `dist`, e execute o comando de instalação apresentado anteriormente.

## Desenvolvendo :cake: :ice_cream:

Para continuar desenvolvendo este projeto, basta utilizar o Pipenv, instalar as dependências e continuar o desenvolvimento. Abaixo é demonstrado como utilizar o Pipenv

```shell
# pip install pipenv 

pipenv install
```

Após instalar, basta realizar o teste :smile:

```shell
pipenv run python api.py
```


# Como utilizar 
Uma vez feito os passos descritos em **Instalação** basta, importar em seu ambiente de desenvolvimento, seguindo o exemplo abaixo:

```
from especialista_fatec.facade import define_empresa_do_usuario

result = define_empresa_do_usuario({
    "idade": 20,
    "nota": 5,
    "opcao": "Internet",
    "tempo": 8
}); print(result)
```
