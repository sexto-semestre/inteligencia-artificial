from setuptools import setup, find_packages

setup(
    name="EspecialistaAPI",
    version="0.1",
    author="FATEC",
    packages=find_packages(),
    zip_safe=False,
)
