from pyknow import (KnowledgeEngine,
                    Rule, P, W, MATCH,
                    OR, NOT)

from especialista_fatec.facts import (IdadeDoUsuario,
                                      NotaIdealDeEmpresaParaOUsuario,
                                      FormaDeContratacaoDoUsuario,
                                      TempoDeRespostaDaEmpresaIdealParaOUsuario,
                                      GrupoDeInteresseDaEmpresa,
                                      TipoDeUsuario, EmpresaEscolhida)


class EngineDeInferenciaDeMelhoresEmpresasDeTelefonia(KnowledgeEngine):
    """Engine de conhecimento para a definição de melhoras empresas
    de telefonia para usuários
    """

    def __init__(self, empresa="Seus critérios não são atendidos por nenhuma empresa"):
        super().__init__()
        self.empresa = empresa

    @Rule(
        IdadeDoUsuario(
            idade=P(lambda idade: idade <= 20)
        )
    )
    def define_grupo_de_interesse_20_anos(self) -> None:
        """Método que é utilizado para a definição do grupo
        de interesse (Até 20 anos)
        """

        self.declare(GrupoDeInteresseDaEmpresa(grupo="ate20"))

    @Rule(
        IdadeDoUsuario(
            idade=P(lambda idade: idade >= 21 and idade <= 30)
        )
    )
    def define_grupo_de_interesse_21_ate_30_anos(self) -> None:
        """Método que é utilizado para a definição do grupo
        de interesse (De 21 até 30 anos)
        """

        self.declare(GrupoDeInteresseDaEmpresa(grupo="de21ate30"))

    @Rule(
        IdadeDoUsuario(
            idade=P(lambda idade: idade >= 31 and idade <= 40)
        )
    )
    def define_grupo_de_interesse_31_ate_40_anos(self) -> None:
        """Método que é utilizado para a definição do grupo
        de interesse (De 31 até 40 anos)
        """

        self.declare(GrupoDeInteresseDaEmpresa(grupo="de31ate40"))

    @Rule(
        NOT(TipoDeUsuario(tipo=W())),
        GrupoDeInteresseDaEmpresa(grupo="ate20"),
        NotaIdealDeEmpresaParaOUsuario(
            nota=5
        ),
        TempoDeRespostaDaEmpresaIdealParaOUsuario(
            tempo=P(lambda tempo: tempo >= 6 and tempo <= 10)
        )
    )
    def define_usuario_tolerante_ate_20(self) -> None:
        """ Tipo de usuário tolerante ate 20 anos
        """

        self.declare(TipoDeUsuario(tipo=TipoDeUsuario.TOLERANTE))

    @Rule(
        NOT(TipoDeUsuario(tipo=W())),
        OR(
            GrupoDeInteresseDaEmpresa(grupo="ate20"),
            GrupoDeInteresseDaEmpresa(grupo="de21ate30")
        ),
        NotaIdealDeEmpresaParaOUsuario(
            nota=P(lambda nota: nota >= 0 and nota <= 4)
        ),
        TempoDeRespostaDaEmpresaIdealParaOUsuario(
            tempo=P(lambda tempo: tempo >= 7 and tempo <= 10)
        )
    )
    def define_usuario_nao_liga_de_20_ate_31(self) -> None:
        """ Tipo de usuário que não liga de 20 ate 31 anos
        """

        self.declare(TipoDeUsuario(tipo=TipoDeUsuario.NAO_LIGA))

    @Rule(
        NOT(TipoDeUsuario(tipo=W())),
        GrupoDeInteresseDaEmpresa(grupo="ate20"),
        NotaIdealDeEmpresaParaOUsuario(
            nota=P(lambda nota: nota >= 4)
        ),
        TempoDeRespostaDaEmpresaIdealParaOUsuario(
            tempo=P(lambda tempo: tempo >= 0 and tempo <= 5)
        )
    )
    def define_usuario_exigente_entre_20(self) -> None:
        """ Tipo de usuário exigente liga entre 20 anos
        """

        self.declare(TipoDeUsuario(tipo=TipoDeUsuario.EXIGENTE))

    @Rule(
        NOT(TipoDeUsuario(tipo=W())),
        GrupoDeInteresseDaEmpresa(grupo="de21ate30"),
        NotaIdealDeEmpresaParaOUsuario(
            nota=5
        ),
        TempoDeRespostaDaEmpresaIdealParaOUsuario(
            tempo=P(lambda tempo: tempo >= 7 and tempo <= 10)
        )
    )
    def define_usuario_tolerante_entre_21_e_30(self) -> None:
        """ Tipo de usuário tolerante entre 21 e 30 anos
        """

        self.declare(TipoDeUsuario(tipo=TipoDeUsuario.TOLERANTE))

    @Rule(
        NOT(TipoDeUsuario(tipo=W())),
        GrupoDeInteresseDaEmpresa(grupo="de21ate30"),
        NotaIdealDeEmpresaParaOUsuario(
            nota=P(lambda nota: nota >= 4 and nota <= 5)
        ),
        TempoDeRespostaDaEmpresaIdealParaOUsuario(
            tempo=P(lambda tempo: tempo >= 0 and tempo <= 6)
        )
    )
    def define_usuario_exigente_entre_21_e_30(self) -> None:
        """ Tipo de usuário exigente liga entre 21 e 30 anos
        """

        self.declare(TipoDeUsuario(tipo=TipoDeUsuario.EXIGENTE))

    @Rule(
        NOT(TipoDeUsuario(tipo=W())),
        GrupoDeInteresseDaEmpresa(grupo="de31ate40"),
        NotaIdealDeEmpresaParaOUsuario(
            nota=P(lambda nota: nota >= 4 and nota <= 5)
        ),
        TempoDeRespostaDaEmpresaIdealParaOUsuario(
            tempo=P(lambda tempo: tempo >= 6 and tempo <= 10)
        )
    )
    def define_usuario_tolerante_entre_31_e_40(self) -> None:
        """ Tipo de usuário tolerante entre 31 e 40 anos
        """

        self.declare(TipoDeUsuario(tipo=TipoDeUsuario.TOLERANTE))

    @Rule(
        NOT(TipoDeUsuario(tipo=W())),
        GrupoDeInteresseDaEmpresa(grupo="de31ate40"),
        NotaIdealDeEmpresaParaOUsuario(
            nota=P(lambda nota: nota >= 0 and nota <= 4)
        ),
        TempoDeRespostaDaEmpresaIdealParaOUsuario(
            tempo=P(lambda tempo: tempo >= 6 and tempo <= 10)
        )
    )
    def define_usuario_nao_liga_entre_31_e_40(self) -> None:
        """ Tipo de usuário que não liga entre 31 e 40 anos
        """

        self.declare(TipoDeUsuario(tipo=TipoDeUsuario.NAO_LIGA))

    @Rule(
        NOT(TipoDeUsuario(tipo=W())),
        GrupoDeInteresseDaEmpresa(grupo="de31ate40"),
        NotaIdealDeEmpresaParaOUsuario(
            nota=P(lambda nota: nota >= 4 and nota <= 5)
        ),
        TempoDeRespostaDaEmpresaIdealParaOUsuario(
            tempo=P(lambda tempo: tempo >= 0 and tempo <= 5)
        )
    )
    def define_usuario_exigente_entre_31_e_40(self) -> None:
        """ Tipo de usuário exigente liga entre 31 e 40 anos
        """

        self.declare(TipoDeUsuario(tipo=TipoDeUsuario.EXIGENTE))

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="ate20"),
        TipoDeUsuario(tipo=TipoDeUsuario.TOLERANTE),
        OR(
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.INTERNET
            ),
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA
            ),
        )
    )
    def regra_1_define_empresa_para_usuarios_tolerantes_ate_20_anos(self) -> None:
        """ Define empresa claro para usuários tolerantes de até 20 anos
        """

        self.empresa = EmpresaEscolhida.CLARO

    @Rule(
        OR(
            GrupoDeInteresseDaEmpresa(grupo="ate20"),
            GrupoDeInteresseDaEmpresa(grupo="de21ate30")
        ),
        TipoDeUsuario(tipo=TipoDeUsuario.NAO_LIGA),
        OR(
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA),
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.SMS),
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.TELEFONE),
        )
    )
    def regra_2_define_empresa_para_usuarios_nao_ligam_ate_30_anos(self) -> None:
        """Define empresa OI para usuários que não ligam de até 30 anos
        """

        self.empresa = EmpresaEscolhida.OI

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="ate20"),
        TipoDeUsuario(tipo=TipoDeUsuario.EXIGENTE),
        FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.SMS)

    )
    def regra_3_define_empresa_para_usuarios_exigentes_ate_20_anos(self) -> None:
        """Define empresa Tim para usuários que são exigentes de até 20 anos
        """

        self.empresa = EmpresaEscolhida.TIM

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="de21ate30"),
        TipoDeUsuario(tipo=TipoDeUsuario.TOLERANTE),
        OR(
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.SMS),
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.INTERNET),
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA),
        )
    )
    def regra_4_define_empresa_para_usuarios_tolerantes_de_21_ate_30_anos(self) -> None:
        """Define empresa Claro para usuários que são tolerantes de 21 à 30 anos
        """

        self.empresa = EmpresaEscolhida.CLARO

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="de21ate30"),
        TipoDeUsuario(tipo=TipoDeUsuario.NAO_LIGA),
        FormaDeContratacaoDoUsuario(opcao=FormaDeContratacaoDoUsuario.SMS),
    )
    def regra_5_define_empresa_para_usuarios_nao_ligam_de_21_ate_30_anos(self) -> None:
        """Define empresa Claro para usuários que não ligam de 21 até 30 anos e contrataram via sms
        """

        self.empresa = EmpresaEscolhida.CLARO

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="de21ate30"),
        TipoDeUsuario(tipo=TipoDeUsuario.NAO_LIGA),
        FormaDeContratacaoDoUsuario(opcao=FormaDeContratacaoDoUsuario.TELEFONE)
    )
    def regra_6_define_empresa_para_usuarios_nao_ligam_de_21_ate_30_anos(self) -> None:
        """Define empresa Oi para usuários que não ligam de 21 até 30 anos e contrataram via telefone
        """

        self.empresa = EmpresaEscolhida.OI

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="de21ate30"),
        TipoDeUsuario(tipo=TipoDeUsuario.EXIGENTE),
        FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA)
    )
    def regra_7_define_empresa_para_usuarios_exigentes_de_21_ate_30_anos(self) -> None:
        """Define empresa Oi para usuários que são exigentes de 21 até 30 anos
        """

        self.empresa = EmpresaEscolhida.OI

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="de31ate40"),
        TipoDeUsuario(tipo=TipoDeUsuario.NAO_LIGA),
        OR(
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.DOMICILIO),
            FormaDeContratacaoDoUsuario(
                opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA)
        )

    )
    def regra_8_define_empresa_para_usuarios_nao_ligam_de_31_ate_40_anos(self) -> None:
        """Define empresa Claro para usuários que são não ligam de 31 até 40 anos
        """

        self.empresa = EmpresaEscolhida.CLARO

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="de31ate40"),
        TipoDeUsuario(tipo=TipoDeUsuario.TOLERANTE),
        FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.INTERNET),
    )
    def regra_9_define_empresa_para_usuarios_tolerantes_de_31_ate_40_anos(self) -> None:
        """Define empresa Claro para usuários tolerantes de 31 até 40 anos
        """

        self.empresa = EmpresaEscolhida.CLARO

    @Rule(
        GrupoDeInteresseDaEmpresa(grupo="de31ate40"),
        TipoDeUsuario(tipo=TipoDeUsuario.EXIGENTE),
        FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.INTERNET)
    )
    def regra_10_define_empresa_para_usuarios_exigentes_de_31_ate_40_anos(self) -> None:
        """Define empresa Claro para usuários exigentes de 31 até 40 anos
        """

        self.empresa = EmpresaEscolhida.TIM
