def verifica_informacoes_do_usuario(infos: dict):
    """Valida o dicionário de inputs do usuário

    Valida o dicionário, caso haja problemas uma exceção é disparada

    Args:
        infos: Dicionário contendo as seguintes chaves:
            idade ``int``: Idade do usuário
            nota ``int``: Nota de excelência da empresa para o usuário (0 a 10)
            opcao ``str``: Forma de contratação
            tempo ``int``: Tempo de resposta máximo para o usuário    
    """

    if {"idade", "nota", "opcao", "tempo"} != set(infos):
        raise TypeError("O dicionário inserido não tem a estrutura válida")
