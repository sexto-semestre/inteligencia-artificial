def define_empresa_do_usuario(infos: dict) -> str:
    """Define a empresa ideal para o usuário com base em suas informações
    
    Faz a definição ideal para o usuário com base nas informações inseridas, 
    caso haja problemas no input, exceções serão lançadas

    Args:
        infos: Dicionário contendo as seguintes chaves:
            idade ``int``: Idade do usuário
            nota ``int``: Nota de excelência da empresa para o usuário (0 a 10)
            opcao ``str``: Forma de contratação
            tempo ``int``: Tempo de resposta máximo para o usuário
    Returns:
        str: Nome da empresa indicada para o usuário
    """
    from especialista_fatec.engines import EngineDeInferenciaDeMelhoresEmpresasDeTelefonia

    from especialista_fatec.facts import (  IdadeDoUsuario, 
                                    NotaIdealDeEmpresaParaOUsuario,
                                    FormaDeContratacaoDoUsuario, 
                                    TempoDeRespostaDaEmpresaIdealParaOUsuario )

    from especialista_fatec.utils import verifica_informacoes_do_usuario

    verifica_informacoes_do_usuario(infos)
    
    engine = EngineDeInferenciaDeMelhoresEmpresasDeTelefonia()
    engine.reset()

    engine.declare(IdadeDoUsuario(idade=infos["idade"]))
    engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=infos["nota"]))
    engine.declare(FormaDeContratacaoDoUsuario(opcao=infos["opcao"]))
    engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=infos["tempo"]))
    engine.run()
    
    return engine.empresa
