from pyknow import Fact

class FormaDeContratacaoDoUsuario(Fact):
    """Fato da forma de compra do usuário. Os valores possíveis de contratação são:

    - Internet;
    - Loja Física;
    - Telefone;
    - SMS;
    - Domicilio.
    """

    INTERNET="Internet"
    LOJA_FISICA="Loja física"
    TELEFONE="Telefone"
    SMS="SMS"
    DOMICILIO="Domicilio"

class IdadeDoUsuario(Fact):
    """Fato da idade do usuário
    """
    pass

class NotaIdealDeEmpresaParaOUsuario(Fact):
    """Fato da nota ideal para o usuário
    """
    pass

class TempoDeRespostaDaEmpresaIdealParaOUsuario(Fact):
    """Fato do tempo de resposta que o usuário aceita das empresas
    """
    pass

class GrupoDeInteresseDaEmpresa(Fact):
    """Fato representando o grupo de interesse da empresa
    """
    pass

class TipoDeUsuario(Fact):
    """Fato com o tipo de usuário. Os tipos de usuário poderão ser

    - Exigente (EX);
    - Tolerante (T);
    - Não classificado (NC);
    - Não liga (NL)
    """

    EXIGENTE="EX"
    TOLERANTE="T"
    NAO_CLASSIFICADO="NC"
    NAO_LIGA="NL"

class EmpresaEscolhida(Fact):
    """Empresa escolhida para o usuário, com base em suas informações
    """

    CLARO="Claro Celular"
    TIM="Tim"
    OI="Oi Celular"
