from unittest import TestCase
from especialista_fatec.engines import EngineDeInferenciaDeMelhoresEmpresasDeTelefonia

from especialista_fatec.facts import (IdadeDoUsuario,
                                      NotaIdealDeEmpresaParaOUsuario,
                                      FormaDeContratacaoDoUsuario,
                                      TempoDeRespostaDaEmpresaIdealParaOUsuario)

from especialista_fatec.utils import verifica_informacoes_do_usuario


class UnitTests(TestCase):

    def setUp(self):
        self.engine = EngineDeInferenciaDeMelhoresEmpresasDeTelefonia()
        self.engine.reset()

    def tearDown(self):
        self.engine = None

    # testes com consumidores de 20 anos

    def test_1_usuario_tolerante_20_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=20))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=5))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=8))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA))
        self.engine.run()
        self.assertEqual("Claro Celular", self.engine.empresa)

    def test_2_usuario_nao_liga_20_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=20))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=4))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=9))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA))
        self.engine.run()
        self.assertEqual("Oi Celular", self.engine.empresa)

    def test_3_usuario_exigente_20_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=20))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=4))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=5))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.SMS))
        self.engine.run()
        self.assertEqual("Tim", self.engine.empresa)

    # Testes com consumidores de 21 à 30 anos

    def test_4_usuario_tolerante_21_a_30_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=21))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=5))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=8))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.INTERNET))
        self.engine.run()
        self.assertEqual("Claro Celular", self.engine.empresa)

    def test_5_usuario_nao_liga_21_a_30_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=21))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=2))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=7))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.SMS))
        self.engine.run()
        self.assertEqual("Claro Celular", self.engine.empresa)

    def test_6_usuario_nao_liga_21_a_30_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=21))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=3))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=7))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.TELEFONE))
        self.engine.run()
        self.assertEqual("Oi Celular", self.engine.empresa)

    def test_7_usuario_exigente_21_a_30_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=21))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=4))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=6))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA))
        self.engine.run()
        self.assertEqual("Oi Celular", self.engine.empresa)

    def test_8_usuario_nao_liga_31_a_40_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=31))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=3))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=8))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA))
        self.engine.run()
        self.assertEqual("Claro Celular", self.engine.empresa)

    def test_9_usuario_tolerante_31_a_40_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=31))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=4))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=8))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.INTERNET))
        self.engine.run()
        self.assertEqual("Claro Celular", self.engine.empresa)

    def test_10_usuario_exigente_31_a_40_anos(self):
        self.engine.declare(IdadeDoUsuario(idade=31))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=4))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=5))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.INTERNET))
        self.engine.run()
        self.assertEqual("Tim", self.engine.empresa)

    def test_11_usuario_tolerante_31_a_40_anos_loja_fisica(self):
        self.engine.declare(IdadeDoUsuario(idade=31))
        self.engine.declare(NotaIdealDeEmpresaParaOUsuario(nota=3))
        self.engine.declare(TempoDeRespostaDaEmpresaIdealParaOUsuario(tempo=8))
        self.engine.declare(FormaDeContratacaoDoUsuario(
            opcao=FormaDeContratacaoDoUsuario.LOJA_FISICA))
        self.engine.run()
        self.assertEqual("Claro Celular", self.engine.empresa)