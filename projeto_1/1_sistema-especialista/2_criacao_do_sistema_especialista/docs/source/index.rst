Documentação da EspecialistaAPI 
================================

Biblioteca de sistema especialista focado na busca de melhores empresas para seus usuários.
Criada durante as aulas de inteligência artificial da Fatec-SJC

.. toctree::
   :maxdepth: 4

   especialista_fatec
