Pacote especialista\_fatec
============================

Módulos do pacote especialista\_fatec, presente na **EspecialistaAPI**


Módulo especialista\_fatec.engines
------------------------------------

.. automodule:: especialista_fatec.engines
    :members:
    :undoc-members:
    :show-inheritance:

Módulo especialista\_fatec.facade
-----------------------------------

.. automodule:: especialista_fatec.facade
    :members:
    :undoc-members:
    :show-inheritance:

Módulo especialista\_fatec.facts
----------------------------------

.. automodule:: especialista_fatec.facts
    :members:
    :undoc-members:
    :show-inheritance:

Módulo  especialista\_fatec.utils
----------------------------------

.. automodule:: especialista_fatec.utils
    :members:
    :undoc-members:
    :show-inheritance:
