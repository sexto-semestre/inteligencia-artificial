# Documentação

Para criar a documentação é necessário já ter a biblioteca do pacote instalada, em seguida, basta utilizar o `Sphinxs` para gerar a documentação, veja

```shell
pip install Sphinx

./build.sh # Dentro do diretório docs
```
