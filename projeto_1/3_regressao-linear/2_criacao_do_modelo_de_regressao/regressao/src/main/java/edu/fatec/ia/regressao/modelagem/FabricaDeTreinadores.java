package edu.fatec.ia.regressao.modelagem;

import java.util.LinkedList;
import java.util.List;

import weka.classifiers.functions.LinearRegression;
import weka.core.Instances;

/**
 * Classe para facilitar a criação de TreinadorDeRegressão, para um conjunto N
 * de regressões
 * 
 * @author felipe
 *
 */
public class FabricaDeTreinadores {

	private Instances dados;
	private Integer porcentagemDeTreino;
	private List<LinearRegression> regressoes;

	public FabricaDeTreinadores(List<LinearRegression> regressoes, Instances dados, Integer porcentagemDeTreino) {
		this.dados = dados;
		this.regressoes = regressoes;
		this.porcentagemDeTreino = porcentagemDeTreino;
	}

	/**
	 * Método para gerar um conjunto de TreinadorDeRegressao, todos com as
	 * regressões já treinadas e configuradas
	 * 
	 * @return
	 * @throws Exception
	 */
	public List<TreinadorDeRegressao> geraOsTreinadoresDeRegressao() throws Exception {

		List<TreinadorDeRegressao> treinadorDeRegressaos = new LinkedList<TreinadorDeRegressao>();

		regressoes.stream().forEach(r -> {
			TreinadorDeRegressao treinadorDeRegressaoTemp = new TreinadorDeRegressao(r, dados, porcentagemDeTreino);
			try {
				treinadorDeRegressaoTemp.treinaModeloDeRegressao();
			} catch (Exception e) {
				e.printStackTrace();
			}
			treinadorDeRegressaos.add(treinadorDeRegressaoTemp);
		});

		return treinadorDeRegressaos;
	}
}
