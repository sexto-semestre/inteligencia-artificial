package edu.fatec.ia.regressao.utils;

import java.util.LinkedList;
import java.util.List;

import weka.classifiers.functions.LinearRegression;
import weka.core.SelectedTag;

/**
 * Classe para facilitar a geração de certas estruturas de dados
 * 
 * @author felipe
 *
 */
public class Gerador {

	/**
	 * Gera conjunto de regressões lineares com as definições do usuário
	 * 
	 * @return
	 * @throws Exception
	 */
	public static List<LinearRegression> geraRegressoes() throws Exception {

		LinearRegression regressor;
		List<LinearRegression> regressoes = new LinkedList<>();

//	Regressao 11:
//  Batch size: 50
		regressor = new LinearRegression();
		regressor.setAttributeSelectionMethod(
				new SelectedTag(LinearRegression.SELECTION_NONE, LinearRegression.TAGS_SELECTION));

		regressor.setBatchSize("50");
		regressor.setRidge(0);
		regressor.setEliminateColinearAttributes(true);
		regressor.setDoNotCheckCapabilities(false);
		regressor.setNumDecimalPlaces(3);
		regressor.setMinimal(false);
		regressor.setUseQRDecomposition(false);
		regressor.setOutputAdditionalStats(false);
		regressor.setDebug(true);
		regressoes.add(regressor);

//		Regressao 12:
//  	Batch size: 100
		regressor = new LinearRegression();
		regressor.setAttributeSelectionMethod(
				new SelectedTag(LinearRegression.SELECTION_NONE, LinearRegression.TAGS_SELECTION));

		regressor.setBatchSize("100");
		regressor.setRidge(0);
		regressor.setEliminateColinearAttributes(true);
		regressor.setDoNotCheckCapabilities(false);
		regressor.setNumDecimalPlaces(3);
		regressor.setMinimal(false);
		regressor.setUseQRDecomposition(false);
		regressor.setOutputAdditionalStats(false);
		regressor.setDebug(true);
		regressoes.add(regressor);

//		Regressao 21:
//	  	greedy 
//		colinear atrr = FALSE
		regressor = new LinearRegression();
		regressor.setAttributeSelectionMethod(
				new SelectedTag(LinearRegression.SELECTION_GREEDY, LinearRegression.TAGS_SELECTION));

		regressor.setBatchSize("100");
		regressor.setRidge(0);
		regressor.setEliminateColinearAttributes(false);
		regressor.setDoNotCheckCapabilities(false);
		regressor.setNumDecimalPlaces(3);
		regressor.setMinimal(false);
		regressor.setUseQRDecomposition(false);
		regressor.setOutputAdditionalStats(false);
		regressor.setDebug(true);
		regressoes.add(regressor);

//			Regressao 22:
//		  	greey 
//			colinear atrr = TRUE
		regressor = new LinearRegression();
		regressor.setAttributeSelectionMethod(
				new SelectedTag(LinearRegression.SELECTION_GREEDY, LinearRegression.TAGS_SELECTION));

		regressor.setBatchSize("100");
		regressor.setRidge(0);
		regressor.setEliminateColinearAttributes(true);
		regressor.setDoNotCheckCapabilities(false);
		regressor.setNumDecimalPlaces(3);
		regressor.setMinimal(false);
		regressor.setUseQRDecomposition(false);
		regressor.setOutputAdditionalStats(false);
		regressor.setDebug(true);
		regressoes.add(regressor);

//			Regressao 31:
//		  	m5
//			colinear atrr = FALSE
		regressor = new LinearRegression();
		regressor.setAttributeSelectionMethod(
				new SelectedTag(LinearRegression.SELECTION_M5, LinearRegression.TAGS_SELECTION));

		regressor.setBatchSize("100");
		regressor.setRidge(0);
		regressor.setEliminateColinearAttributes(false);
		regressor.setDoNotCheckCapabilities(false);
		regressor.setNumDecimalPlaces(3);
		regressor.setMinimal(false);
		regressor.setUseQRDecomposition(false);
		regressor.setOutputAdditionalStats(false);
		regressor.setDebug(true);
		regressoes.add(regressor);

//			Regressao 32:
//		  	m5
//			colinear atrr = TRUE
		regressor = new LinearRegression();
		regressor.setAttributeSelectionMethod(
				new SelectedTag(LinearRegression.SELECTION_M5, LinearRegression.TAGS_SELECTION));

		regressor.setBatchSize("100");
		regressor.setRidge(0);
		regressor.setEliminateColinearAttributes(true);
		regressor.setDoNotCheckCapabilities(false);
		regressor.setNumDecimalPlaces(3);
		regressor.setMinimal(false);
		regressor.setUseQRDecomposition(false);
		regressor.setOutputAdditionalStats(false);
		regressor.setDebug(true);
		regressoes.add(regressor);

//			Regressao 41:
//		  	none
//			colinear atrr = FALSE
		regressor = new LinearRegression();
		regressor.setAttributeSelectionMethod(
				new SelectedTag(LinearRegression.SELECTION_NONE, LinearRegression.TAGS_SELECTION));

		regressor.setBatchSize("100");
		regressor.setRidge(0);
		regressor.setEliminateColinearAttributes(false);
		regressor.setDoNotCheckCapabilities(false);
		regressor.setNumDecimalPlaces(3);
		regressor.setMinimal(false);
		regressor.setUseQRDecomposition(false);
		regressor.setOutputAdditionalStats(false);
		regressor.setDebug(true);
		regressoes.add(regressor);

//			Regressao 42:
//		  	m5
//			colinear atrr = TRUE
		regressor = new LinearRegression();
		regressor.setAttributeSelectionMethod(
				new SelectedTag(LinearRegression.SELECTION_NONE, LinearRegression.TAGS_SELECTION));

		regressor.setBatchSize("100");
		regressor.setRidge(0);
		regressor.setEliminateColinearAttributes(true);
		regressor.setDoNotCheckCapabilities(false);
		regressor.setNumDecimalPlaces(3);
		regressor.setMinimal(false);
		regressor.setUseQRDecomposition(false);
		regressor.setOutputAdditionalStats(false);
		regressor.setDebug(true);
		regressoes.add(regressor);

		return regressoes;
	}
}
