package edu.fatec.ia.regressao.modelagem;

import java.util.HashMap;
import java.util.Map;

import edu.fatec.ia.regressao.utils.ManipuladorDeDados;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.LinearRegression;
import weka.core.Instances;

/**
 * Classe de manipulação do treinamento de regressões lineares
 * 
 * @author felipe
 *
 */
public class TreinadorDeRegressao {

	private LinearRegression regressaoLinear;
	private Integer porcentagemDeTreino;
	private Instances dados;
	private Map<String, Instances> mapaDeDados = null;

	public TreinadorDeRegressao(Instances dados, Integer porcentagemDeTreino) {
		this.porcentagemDeTreino = porcentagemDeTreino;
		this.regressaoLinear = null;
		this.dados = dados;
	}

	public TreinadorDeRegressao(LinearRegression regressaoLinear, Instances dados, Integer porcentagemDeTreino) {
		this.porcentagemDeTreino = porcentagemDeTreino;
		this.regressaoLinear = regressaoLinear;
		this.dados = dados;
	}

	public void treinaModeloDeRegressao() throws Exception {
		verificaModelo();
		divideOsDados();

		regressaoLinear.buildClassifier(mapaDeDados.get("treino"));
	}

	public LinearRegression getRegressaoLinear() {
		return regressaoLinear;
	}

	public void setRegressaoLinear(LinearRegression regressaoLinear) {
		this.regressaoLinear = regressaoLinear;
	}

	public Instances getDados() {
		return dados;
	}

	public void setDados(Instances dados) {
		this.dados = dados;
	}

	public Map<String, Evaluation> avaliaModelo() throws Exception {
		verificaModelo();

		divideOsDados();
		Map<String, Evaluation> mapaDeAvaliacao = new HashMap<String, Evaluation>();

		// Preparando os dados
		Evaluation avalTreino = new Evaluation(mapaDeDados.get("treino"));
		Evaluation avalTeste = new Evaluation(mapaDeDados.get("teste"));

		// Avaliando o modelo
		avalTreino.evaluateModel(regressaoLinear, mapaDeDados.get("treino"));
		avalTeste.evaluateModel(regressaoLinear, mapaDeDados.get("teste"));

		mapaDeAvaliacao.put("treino", avalTreino);
		mapaDeAvaliacao.put("teste", avalTeste);

		return mapaDeAvaliacao;
	}

	private void verificaModelo() {
		if (regressaoLinear == null) {
			throw new NullPointerException("Modelo de regressão não está definido!");
		}
	}

	/**
	 * Método para separar os dados em treino e teste
	 */
	private void divideOsDados() {
		if (mapaDeDados == null) {
			mapaDeDados = ManipuladorDeDados.divideConjuntoDeDados(dados, porcentagemDeTreino);
		}
	}
}
