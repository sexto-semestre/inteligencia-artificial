package edu.fatec.ia.regressao.utils;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import weka.core.Instances;
import weka.core.converters.CSVLoader;

/**
 * Utilitário de leitura dos dados
 * 
 * @author felipe
 *
 */
public class ManipuladorDeDados {

	/**
	 * Método para ler um dataset em CSV
	 * 
	 * @param caminho
	 * @param atributoDependente
	 * @param atributoNaoRelevantes
	 * @return
	 * @throws Exception
	 */
	public static Instances carregaDadosCSV(String caminho, String nomeDoAtribuitoDependente,
			List<String> atributosNaoRelevantes) throws Exception {

		CSVLoader csvLoader = new CSVLoader();
		csvLoader.setSource(new File(caminho));

		Instances dataIntances = csvLoader.getDataSet();

		dataIntances.setClassIndex(dataIntances.attribute(nomeDoAtribuitoDependente).index());

		if (atributosNaoRelevantes != null) {
			atributosNaoRelevantes.stream().forEach(atributo -> {
				dataIntances.deleteAttributeAt(dataIntances.attribute(atributo).index());
			});
		}

		return dataIntances;
	}

	/**
	 * Método para dividir o conjunto de dados em treino e teste
	 * 
	 * @param dados
	 * @param porcentagemDeTreino
	 * @return
	 */
	public static Map<String, Instances> divideConjuntoDeDados(Instances dados, int porcentagemDeTreino) {

		HashMap<String, Instances> mapaDeDados = new HashMap<String, Instances>();

		int elementosDeTreino = Math.round((dados.numInstances() * porcentagemDeTreino) / 100);
		int elementosDeTeste = dados.numInstances() - elementosDeTreino;

		mapaDeDados.put("treino", new Instances(dados, 0, elementosDeTreino));
		mapaDeDados.put("teste", new Instances(dados, elementosDeTreino, elementosDeTeste));

		return mapaDeDados;
	}
}
