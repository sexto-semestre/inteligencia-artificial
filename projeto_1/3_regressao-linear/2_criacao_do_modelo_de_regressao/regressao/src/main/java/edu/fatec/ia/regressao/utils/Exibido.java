package edu.fatec.ia.regressao.utils;

import java.util.List;
import java.util.Map;

import edu.fatec.ia.regressao.modelagem.TreinadorDeRegressao;
import weka.classifiers.Evaluation;

public class Exibido {

	public static void exibeEstatisticas(List<TreinadorDeRegressao> treinadoresDeRegressao) {
		treinadoresDeRegressao.stream().forEach(x -> {
			Map<String, Evaluation> avaliacoes;
			try {
				avaliacoes = x.avaliaModelo();	
				
				// Exibindo avaliações
				System.out.println("----------------------------------------------------");
			
				System.out.println(">>>\tParametros\n");
				
				System.out.print("Metodo: ");
				System.out.println(x.getRegressaoLinear().getAttributeSelectionMethod().getSelectedTag().getReadable());

				System.out.print("Batch Size: ");
				System.out.println(x.getRegressaoLinear().getBatchSize());
				
				System.out.print("QRDecomposition: ");
				System.out.println(x.getRegressaoLinear().getUseQRDecomposition());

				System.out.print("Eliminate Colinear Attributes: ");
				System.out.println(x.getRegressaoLinear().getEliminateColinearAttributes());
				
				System.out.print("Ridge: ");
				System.out.println(x.getRegressaoLinear().getRidge());
				
				System.out.print("Minimal: ");
				System.out.println(x.getRegressaoLinear().getMinimal());

				System.out.print("\n>>>\tMODELO: ");
				System.out.println(x.getRegressaoLinear().toString());
				
				System.out.println("\n>>>\tAvaliação de treino");
				System.out.println(avaliacoes.get("treino").toSummaryString());			
				
				System.out.println(">>>\tAvaliação de teste");
				System.out.println(avaliacoes.get("teste").toSummaryString());
				
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
	}
}
