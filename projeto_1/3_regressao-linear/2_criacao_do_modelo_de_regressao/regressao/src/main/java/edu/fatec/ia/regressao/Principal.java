package edu.fatec.ia.regressao;

import java.util.ArrayList;
import java.util.List;

import edu.fatec.ia.regressao.modelagem.FabricaDeTreinadores;
import edu.fatec.ia.regressao.utils.Exibido;
import edu.fatec.ia.regressao.utils.Gerador;
import edu.fatec.ia.regressao.utils.ManipuladorDeDados;
import weka.classifiers.functions.LinearRegression;
import weka.core.Instances;

/*
 * x-cliente	= 100 % 
 * x-distac 	= 70 %
 * x-distal		= 70 % (inverso)
 * x-idade		= 0%
 * x-ndomic		= 5%
 * x-renda		= 20%
 * */

public class Principal {
	public static void main(String[] args) throws Exception {

		// nclien, ndomic, renda, idade, distac, distal
		String atributoDependente = "nclien";

		List<String> atributosNaoRelevantes = new ArrayList<>();
		atributosNaoRelevantes.add("renda");
		atributosNaoRelevantes.add("ndomic");
		atributosNaoRelevantes.add("idade");

		Instances dadosSelecionados = ManipuladorDeDados.carregaDadosCSV("res/paulaeg426Novo.csv", atributoDependente,
				atributosNaoRelevantes);
		Instances dadosGerais = ManipuladorDeDados.carregaDadosCSV("res/paulaeg426Novo.csv", atributoDependente, null);

		List<LinearRegression> regressoes = Gerador.geraRegressoes();

		FabricaDeTreinadores fabricaDeTreinadoresDadosGerais = new FabricaDeTreinadores(regressoes, dadosGerais, 70);
		FabricaDeTreinadores fabricaDeTreinadoresDadosSelecionados = new FabricaDeTreinadores(regressoes,
				dadosSelecionados, 70);

		System.out.println("\n\n>>>\tDADOS GERAIS\n\n");
		Exibido.exibeEstatisticas(fabricaDeTreinadoresDadosGerais.geraOsTreinadoresDeRegressao());

		System.out.println("\n\n>>>\tDADOS SELECIONADOS\n\n");
		Exibido.exibeEstatisticas(fabricaDeTreinadoresDadosSelecionados.geraOsTreinadoresDeRegressao());

	}
}